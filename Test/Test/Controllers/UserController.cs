﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Test.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private IUserBusiness userBusiness;

        public UserController(IUserBusiness userBusiness)
        {
            this.userBusiness = userBusiness;
        }

        [Route("getall")]
        public List<User> GetAllUsers()
        {
            return this.userBusiness.GetAllUsers();
        }
        [Route("{name}/getuserbyname")]
        public List<User> GetUserByName(string name)
        {
            return this.userBusiness.GetUserByName(name);
        }
        [Route("insert")]
        [HttpPost]
        public bool InsertUser([FromBody]User u)
        {
            return this.userBusiness.InsertUser(u);
        }
        [Route("update")]
        [HttpPut]
        public bool UpdateUser([FromBody]User u)
        {
            return this.userBusiness.UpdateUser(u);
        }

        [Route("{id}/delete")]
        [HttpDelete]
        public bool DeleteUser(int id)
        {
            return this.userBusiness.DeleteUser(id);
        }
    }
}
