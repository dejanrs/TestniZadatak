﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class UserRepository:IUserRepository
    {
        private string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

        public List<User> GetAllUsers()
        {
            List<User> listToReturn = new List<User>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Users";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    User u = new User();
                    u.id = dataReader.GetInt32(0);
                    u.Name = dataReader.GetString(1);
                    u.Surname = dataReader.GetString(2);
                    u.username = dataReader.GetString(3);
                    u.Age = dataReader.GetInt32(4);
                    listToReturn.Add(u);
                }
            }
            return listToReturn;
        }
        public int InsertUser(User u)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "INSERT INTO Users VALUES('"+u.Name+"','"+u.Surname+"','"+u.username+"',"+u.Age+")";

                return command.ExecuteNonQuery();
            }
        }
        public int UpdateUser(User u)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "UPDATE Users SET Name='"+u.Name+"', Surname='"+u.Surname+ "',Username='" + u.username+"',Age="+u.Age+" WHERE Id="+u.id+"";

                return command.ExecuteNonQuery();
            }
        }
        public int DeleteUser(int id)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "DELETE FROM Users WHERE Id = " + id;
                return command.ExecuteNonQuery();
            }
        }

    }
}
