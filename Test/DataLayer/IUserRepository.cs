﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        int InsertUser(User u);
        int UpdateUser(User u);
        int DeleteUser(int id);
    }
}
