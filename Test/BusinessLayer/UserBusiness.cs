﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class UserBusiness:IUserBusiness
    {
        private IUserRepository userRepository;

        public UserBusiness(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public List<User> GetAllUsers()
        {
            List<User> users = this.userRepository.GetAllUsers();
            if (users.Count > 0)
            {
                return users;
            }
            else
            {
                return null;
            }
        }
        public List<User> GetUserByName(string name)
        {
            List<User> users = this.userRepository.GetAllUsers();
            if (users.Count > 0)
            {
                return users.Where(s => s.Name.Contains(name)).ToList();
            }
            else
            {
                return null;
            }
        }
        public bool InsertUser(User u)
        {
            if (this.userRepository.InsertUser(u) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateUser(User u)
        {
            bool result = false;
            if (u.id != 0 && this.userRepository.UpdateUser(u) > 0)
            {
                result = true;
            }
            return result;
        }
        public bool DeleteUser(int id)
        {
            bool result = false; 
            if (id != 0 && this.userRepository.DeleteUser(id) > 0)
            {
                result = true;
            }
            return result;
        }
    }
}
