﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IUserBusiness
    {
        List<User> GetAllUsers();
        bool InsertUser(User u);
        bool UpdateUser(User u);
        bool DeleteUser(int id);
        List<User> GetUserByName(string name);
    }
}
